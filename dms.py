#!/usr/bin/env python

from bottle import Bottle

__version__ = '1.0.0'
__author__ = 'Jesus Vedasto Olazo'


app = Bottle()

@app.route('/')
def index_page():
    return "This is the home page."

if __name__ == '__main__':
    app.run(host='localhost', port='8080', debug=True, reloader=True, server='paste')
